﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IOptions<MongoOptions> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            _database = client.GetDatabase(options.Value.DatabaseName);
        }

        public async void InitializeDb()
        {
            var preferences = await DropAndCreateCollectionAsync<Preference>();
            var customers = await DropAndCreateCollectionAsync<Customer>();

            await preferences.InsertManyAsync(FakeDataFactory.Preferences);
            await customers.InsertManyAsync(FakeDataFactory.Customers);
        }

        private async Task<IMongoCollection<TEntity>> DropAndCreateCollectionAsync<TEntity>()
            where TEntity : BaseEntity
        {
            var collectionName = $"{typeof(TEntity)}s";
            await _database.DropCollectionAsync(collectionName);
            await _database.CreateCollectionAsync(collectionName);

            return _database.GetCollection<TEntity>(collectionName);
        }
    }
}